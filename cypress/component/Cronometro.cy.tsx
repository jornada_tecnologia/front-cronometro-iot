import {mount} from "cypress/react18";
import Cronometro from "@/component/cronometro";
import TemposRegistrados from "@/component/tempo_registrado";

describe('Cronometro Tests', () => {
  it('Deve iniciar com 00:00:00.000', () => {
    cy.visit('http://localhost:3000/cronometro');
    cy.contains('00:00:00.000').should('exist');
  });

  // it('Deve atualizar o tempo quando iniciado', () => {
  //   cy.visit('http://localhost:3000/cronometro');
  //   cy.get('button').contains('Iniciar').click();
  //   cy.wait(1000); // Espera 1 segundo
  //   cy.contains('00:00:01.000').should('not.exist'); // Verifica se o cronômetro não está em 00:00:01.000 (devido à atualização por milissegundos)
  // });
  //
  // it('Deve registrar um tempo', () => {
  //   cy.visit('http://localhost:3000/cronometro');
  //   cy.get('button').contains('Iniciar').click();
  //   cy.wait(1000); // Espera 1 segundo
  //   cy.get('button').contains('Registrar Tempo').click();
  //   cy.wait(200); // Espera 1 segundo
  //   cy.get('ul').find('li').should('have.length', 1); // Verifica se um tempo foi registrado
  // });
  //
  // it('Deve parar o tempo quando parado', () => {
  //   cy.visit('http://localhost:3000');
  //   cy.get('button').contains('Iniciar').click();
  //   cy.wait(1000);
  //   cy.get('button').contains('Parar').click();
  //   cy.wait(1000); // Espera 1 segundo
  //   cy.get('span').invoke('text').then((texto1) => {
  //     cy.wait(1000);
  //     cy.get('span').invoke('text').should('eq', texto1); // Verifica se o tempo permanece o mesmo
  //   });
  // });
});

export {}
