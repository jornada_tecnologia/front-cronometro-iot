export default function TemposRegistrados({ tempos }: any) {
    return (
        <div>
            <h2>Tempos Registrados</h2>
            <ul>
                {tempos.map((tempo: any, index: number) => (
                    <li key={index}>{tempo}</li>
                ))}
            </ul>
        </div>
    );
}
