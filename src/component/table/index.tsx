const people = [
    { name: 'Zeri', team: 'Wickedbotz', t1: 12334, t2:"invalid", t3:"invalid", t4: 44343, t5: 1313, t6: 13123, t7: 123123, t8: 131231, t9: 12312 },
    // More people...
]

export default function TailwindTable() {
    return (
        <div className="px-4 sm:px-6 lg:px-8 mt-8">
            <table class="table-fixed">
            <thead>
            <tr>
                <th>Song</th>
                <th>Artist</th>
                <th>Year</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>The Sliding Mr. Bones (Next Stop, Pottersville)</td>
                <td>Malcolm Lockyer</td>
                <td>1961</td>
            </tr>
            <tr>
                <td>Witchy Woman</td>
                <td>The Eagles</td>
                <td>1972</td>
            </tr>
            <tr>
                <td>Shining Star</td>
                <td>Earth, Wind, and Fire</td>
                <td>1975</td>
            </tr>
            </tbody>
        </table>
        </div>
    )
}
