import { useState, useEffect } from 'react';

export default function Cronometro({ onTempoRegistrado }: any) {
    const [tempo, setTempo] = useState(0);
    const [ativo, setAtivo] = useState(false);

    useEffect(() => {
        let intervalo: any;
        if (ativo) {
            intervalo = setInterval(() => {
                setTempo((tempoAnterior) => tempoAnterior + 1);
            }, 1); // Atualiza a cada milissegundo
        } else {
            clearInterval(intervalo);
        }
        return () => clearInterval(intervalo);
    }, [ativo]);

    const formatarTempo = (milissegundos: any) => {
        let horas = Math.floor(milissegundos / 3600000);
        let minutos = Math.floor((milissegundos % 3600000) / 60000);
        let segundos = Math.floor((milissegundos % 60000) / 1000);
        let milissegundosRestantes = milissegundos % 1000;

        return `${horas.toString().padStart(2, '0')}:${minutos.toString().padStart(2, '0')}:${segundos.toString().padStart(2, '0')}.${milissegundosRestantes.toString().padStart(3, '0')}`;
    };

    return (
        <div>
            <h1>Cronômetro</h1>
            <span>{formatarTempo(tempo)}</span>
            <button onClick={() => setAtivo(!ativo)}>
                {ativo ? 'Parar' : 'Iniciar'}
            </button>
            <button onClick={() => onTempoRegistrado(tempo)}>Registrar Tempo</button>
        </div>
    );
}
