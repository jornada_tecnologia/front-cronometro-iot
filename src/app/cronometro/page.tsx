"use client"
import '../globals.css'

import React, { useState } from 'react';
import TemposRegistrados from "@/component/tempo_registrado";
import Cronometro from "@/component/cronometro";

function CronometroPage() {
    const [tempos, setTempos] : any= useState([]);

    const adicionarTempo = (tempo: any) => {
        if (tempos.length < 9) {
            setTempos([...tempos, tempo]);
        } else {
            alert("Limite de tomadas de tempo atingido");
        }
    };

    return (
       <>
           <div>
               <main>
                   <Cronometro onTempoRegistrado={adicionarTempo} />
                   <TemposRegistrados tempos={tempos} />
               </main>
           </div>


       </>
    );
}
export default CronometroPage;
